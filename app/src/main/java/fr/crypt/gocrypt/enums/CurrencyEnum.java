package fr.crypt.gocrypt.enums;

import java.io.Serializable;

public enum CurrencyEnum implements Serializable{

    BITCOIN("Bitcoin", "BTC"),
    LITCOIN("Litecoin", "LTC"),
    EHTEREUM("Ethereum", "ETH");

    private String longname;
    private String shortname;

    //Constructeur
    CurrencyEnum(String longname, String shortname) {
        this.longname = longname;
        this.shortname = shortname;
    }

    public String getLongname() {
        return longname;
    }

    public String getShortname() {
        return shortname;
    }

    public CurrencyEnum get(String longname){
        return valueOf(longname);
    }

    @Override
    public String toString() {
        return getLongname() + " (" + getShortname() + ")";
    }
}

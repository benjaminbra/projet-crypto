package fr.crypt.gocrypt;

import java.io.Serializable;

public class Currency implements Serializable {

    public final static String BITCOIN = "Bitcoin";
    public final static String LITCOIN = "Litecoin";
    public final static String EHTEREUM = "Ethereum";

    private String title;
    private float sold;
    private float moneyValue;
    private float percent;
    private boolean evolution; //true : ▲ , false : ▼
    private String ImageEvol;
    private String Image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getSold() {
        return sold;
    }

    public void setSold(float sold) {
        this.sold = sold;
    }

    public float getMoneyValue() {
        return moneyValue;
    }

    public void setMoneyValue(float moneyValue) {
        this.moneyValue = moneyValue;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public boolean getEvolution() {
        return evolution;
    }

    public void setEvolution(boolean evolution) {


            this.evolution = evolution;

    }

    public void setImage(String title){
        if (title == BITCOIN){
            this.Image = "bitcoin.png";
        }
        if (title == EHTEREUM){
            this.Image = "ethereum.png";
        }
        if (title == LITCOIN){
            this.Image = "litcoin.png";
        }
    }
}

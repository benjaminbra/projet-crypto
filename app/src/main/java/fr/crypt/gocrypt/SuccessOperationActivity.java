package fr.crypt.gocrypt;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class SuccessOperationActivity extends Activity {

    Activity context;

    public final static int SUCCESS_OPERATION = 1;

    public final static int FAIL_OPERATION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_operation);

        ImageButton returnCoins = findViewById(R.id.returnCoins2);
        returnCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(SUCCESS_OPERATION);
                finish();
            }
        });
    }
}

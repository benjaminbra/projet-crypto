package fr.crypt.gocrypt.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.crypt.gocrypt.Currency;
import fr.crypt.gocrypt.R;
import fr.crypt.gocrypt.enums.CurrencyEnum;


public class CurrencyList extends ArrayAdapter<Currency> {

    List<Currency> itemCurrency;


    public CurrencyList(Context context, int resource, List<Currency> items) {
        super(context, resource, items);

        itemCurrency = items;

    }

    @Override
    public int getCount() {
        return this.itemCurrency.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_currency, null);
        }

        Currency p = getItem(position);

        if (p != null) {
            TextView currenName = (TextView) v.findViewById(R.id.currenName);
            TextView currentSold = (TextView) v.findViewById(R.id.currentSold);
            TextView soldDollar = (TextView) v.findViewById(R.id.soldDollar);
            TextView percentEvol = (TextView) v.findViewById(R.id.percentEvol);
            TextView evolution = (TextView) v.findViewById(R.id.evolArrow);
            ImageView ImgCurrent = (ImageView) v.findViewById(R.id.imageView);

            TextView percentSymb = (TextView) v.findViewById(R.id.percentSymbole);

            if (currenName != null) {
                currenName.setText(p.getTitle());
                if (CurrencyEnum.BITCOIN.getLongname().equals(p.getTitle())) {
                    ImgCurrent.setImageResource(R.drawable.bitcoin);
                } else if (CurrencyEnum.EHTEREUM.getLongname().equals(p.getTitle())) {
                    ImgCurrent.setImageResource(R.drawable.ethereum);
                } else if (CurrencyEnum.LITCOIN.getLongname().equals(p.getTitle())) {
                    ImgCurrent.setImageResource(R.drawable.litcoin);
                }
            }

            if (currentSold != null) {
                currentSold.setText(String.valueOf(p.getSold()));
            }

            if (soldDollar != null) {
                soldDollar.setText(String.valueOf(p.getMoneyValue()));
            }

            if (percentEvol != null) {
                percentEvol.setText(String.valueOf(p.getPercent()));
            }
            if (p.getEvolution() == true) {
                evolution.setText(String.valueOf("▲"));
            } else {
                evolution.setText(String.valueOf("▼"));
                evolution.setTextColor(Color.parseColor("#fe1414"));
                percentEvol.setTextColor(Color.parseColor("#fe1414"));
                percentSymb.setTextColor(Color.parseColor("#fe1414"));
            }
        }

        return v;
    }

}


package fr.crypt.gocrypt.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import fr.crypt.gocrypt.Coin;
import fr.crypt.gocrypt.R;

public class CoinAdapter extends ArrayAdapter<Coin> {

    List<Coin> coins;

    public CoinAdapter(@NonNull Context context, int resource, @NonNull List<Coin> objects) {
        super(context, resource, objects);
        this.coins = objects;
    }

    @Override
    public int getCount() {
        return this.coins.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_coin,null);
        }

        Coin c = this.coins.get(position);

        if(c != null){
            ImageView soldImage = v.findViewById(R.id.stateImage);
            TextView soldText = v.findViewById(R.id.stateName);
            TextView dateAction = v.findViewById(R.id.dateTransaction);
            TextView coinCurrency = v.findViewById(R.id.coinCurrency);
            TextView coinAmount = v.findViewById(R.id.coinAmount);
            TextView realCurrency = v.findViewById(R.id.realCurrency);
            TextView realAmount = v.findViewById(R.id.realAmount);
            if(c.isSold()){
                soldImage.setImageResource(R.drawable.oval10);
                soldText.setText("Sold");

                coinAmount.setTextColor(getContext().getResources().getColor(R.color.sold));
                coinCurrency.setTextColor(getContext().getResources().getColor(R.color.sold));
            } else {
                soldImage.setImageResource(R.drawable.oval10copy);
                soldText.setText("Bought");
                coinAmount.setTextColor(getContext().getResources().getColor(R.color.bought));
                coinCurrency.setTextColor(getContext().getResources().getColor(R.color.bought));
            }
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, y");
            dateAction.setText(sdf.format(c.getDate()).toString());

            coinAmount.setText(String.format("%."+(String.valueOf(c.getCoin()).split("\\.")[1].length()+1)+"f\n",c.getCoin()));
            coinCurrency.setText(c.getCoinType());
            realCurrency.setText("$");
            realAmount.setText(String.valueOf(c.getPrice()));
        }

        return v;
    }
}

package fr.crypt.gocrypt.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.crypt.gocrypt.Coin;
import fr.crypt.gocrypt.Currency;
import fr.crypt.gocrypt.R;
import fr.crypt.gocrypt.adapter.CoinAdapter;
import fr.crypt.gocrypt.enums.CurrencyEnum;

public class MoneyListActivity extends Activity {

    Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_list);
        context = this;
        Intent intent = context.getIntent();
        Currency currency = (Currency) intent.getExtras().get(MainActivity.CURRENCY);

        ImageButton returnButton = findViewById(R.id.returnCurrency);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button storeButton = findViewById(R.id.currencyStore);
        Button currencyButton = findViewById(R.id.currencyButton);
        TextView currencyName = findViewById(R.id.currencyName);
        TextView shortCurrencyText = findViewById(R.id.shortCurrencyText);
        String shortCurrency = "";
        //TODO faire quelque chose de plus propre afin d'éviter de la répétition de code
        if (CurrencyEnum.BITCOIN.getLongname().equals(currency.getTitle())) {

            storeButton.setBackgroundResource(R.drawable.roundbitcoin);
            currencyButton.setBackgroundResource(R.drawable.roundbitcoin);
            currencyName.setText(CurrencyEnum.BITCOIN.toString());
            shortCurrencyText.setText(CurrencyEnum.BITCOIN.getShortname());
            shortCurrency = CurrencyEnum.BITCOIN.getShortname();

        } else if (CurrencyEnum.EHTEREUM.getLongname().equals(currency.getTitle())) {

            storeButton.setBackgroundResource(R.drawable.roundethereum);
            currencyButton.setBackgroundResource(R.drawable.roundethereum);
            currencyName.setText(CurrencyEnum.EHTEREUM.toString());
            shortCurrencyText.setText(CurrencyEnum.EHTEREUM.getShortname());
            shortCurrency = CurrencyEnum.EHTEREUM.getShortname();

        } else if (CurrencyEnum.LITCOIN.getLongname().equals(currency.getTitle())) {

            storeButton.setBackgroundResource(R.drawable.roundlitcoin);
            currencyButton.setBackgroundResource(R.drawable.roundlitcoin);
            currencyName.setText(CurrencyEnum.LITCOIN.toString());
            shortCurrencyText.setText(CurrencyEnum.LITCOIN.getShortname());
            shortCurrency = CurrencyEnum.LITCOIN.getShortname();

        }

        storeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shopIntent = new Intent(context, ShopActivity.class);
                startActivity(shopIntent);
            }
        });

        List<Coin> coinList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        // TODO Insérer une vraie liste via une API
        try {
            coinList.add(createCoin(0.000344D, shortCurrency, sdf.parse("5/05/2017"), 1233.45F, true));
            coinList.add(createCoin(0.00045D, shortCurrency, sdf.parse("30/05/2016"), 1500.45F, false));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("5/06/2015"), 1233.45F, true));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("9/06/2014"), 1233.45F, false));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("5/07/2013"), 1233.45F, true));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("5/06/2012"), 1233.45F, true));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("28/06/2011"), 1233.45F, false));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("5/07/2010"), 1233.45F, true));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("12/06/2009"), 1233.45F, true));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("5/06/2008"), 1233.45F, false));
            coinList.add(createCoin(0.00034D, shortCurrency, sdf.parse("5/07/2007"), 1233.45F, true));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        CoinAdapter coinAdapter = new CoinAdapter(this, R.layout.item_coin, coinList);

        ListView listeSoldes = findViewById(R.id.listeSolde);
        listeSoldes.setAdapter(coinAdapter);
    }

    public Coin createCoin(double amount, String type, Date date, float price, boolean sold) {
        Coin coin = new Coin();
        coin.setCoin(amount);
        coin.setCoinType(type);
        coin.setDate(date);
        coin.setPrice(price);
        coin.setSold(sold);
        return coin;
    }
}

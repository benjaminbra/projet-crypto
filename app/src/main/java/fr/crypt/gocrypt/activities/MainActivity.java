package fr.crypt.gocrypt.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import fr.crypt.gocrypt.Currency;
import fr.crypt.gocrypt.R;
import fr.crypt.gocrypt.adapter.CurrencyList;
import fr.crypt.gocrypt.enums.CurrencyEnum;

public class MainActivity extends Activity {

    Activity context;

    final static String CURRENCY = "currency";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);

        final List<Currency> currencies = new ArrayList<>();
        currencies.add(createCurrency(true, 18457.98F, 2.51F, 2.000458F, CurrencyEnum.BITCOIN.getLongname()));
        currencies.add(createCurrency(false, 4872.38F, 8.44F, 0.075693F, CurrencyEnum.LITCOIN.getLongname()));
        currencies.add(createCurrency(true, 10828.65F, 1.58F, 2.45863F, CurrencyEnum.EHTEREUM.getLongname()));

        CurrencyList currencyList = new CurrencyList(this, R.layout.item_currency, currencies);

        TextView totalCurrency = findViewById(R.id.totalCurrencies);
        totalCurrency.setText(String.format("%.2f",getSum(currencies)));

        final ListView listView = findViewById(R.id.ListCur);
        listView.setAdapter(currencyList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                Currency selectCurrency = currencies.get(position);
                bundle.putSerializable(CURRENCY, selectCurrency);

                Intent intent = new Intent(context, MoneyListActivity.class);
                intent.putExtras(bundle);

                startActivity(intent);
            }
        });
    }

    public Currency createCurrency(Boolean evolution, Float money, Float percent, Float sold, String title) {
        Currency currency = new Currency();

        currency.setEvolution(evolution);
        currency.setImage(title);
        currency.setMoneyValue(money);
        currency.setPercent(percent);
        currency.setSold(sold);
        currency.setTitle(title);

        return currency;
    }

    private Float getSum(List<Currency> currencies) {
        Float sum = 0F;
        for (Currency currency : currencies) {
            sum += currency.getMoneyValue();
        }
        return sum;
    }
}

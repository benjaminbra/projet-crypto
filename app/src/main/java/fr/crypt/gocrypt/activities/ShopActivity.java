package fr.crypt.gocrypt.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import fr.crypt.gocrypt.R;
import fr.crypt.gocrypt.SuccessOperationActivity;

public class ShopActivity extends Activity {

    Activity context;

    public final static int SHOPACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        context = this;

        Button buttonSuccess = findViewById(R.id.successButton);
        buttonSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SuccessOperationActivity.class);

                startActivityForResult(intent,SHOPACTIVITY);
            }
        });

        ImageButton returnCoins = findViewById(R.id.returnCoins);
        returnCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SHOPACTIVITY && resultCode == SuccessOperationActivity.SUCCESS_OPERATION){
            finish();
        }
    }
}
